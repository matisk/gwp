// original elements
const content = document.getElementById("content");
const payeeInput = document.getElementById("edit-payee-0-target-id");
const worthWrapper = document.getElementById("edit-worth-wrapper");
const payerWrapper = document.getElementById("edit-payer-0-target-id");
const descriptionInput = document.getElementById("edit-description-0-value");
const br = content.getElementsByTagName("br");
const p = document.getElementsByTagName("p");
const editActions = document.getElementById("edit-actions");
const editSubmit = document.getElementById("edit-submit");

// added elements
const payerTwig = document.getElementById("payer");
const payerWrapperTwig = document.getElementById("payerWrapper");
const worthInteger = document.getElementById("worthInteger");
const worthDecimal = document.getElementById("worthDecimal");
const worthSelect = document.getElementById("worthSelect");
const payerSelect = document.getElementById("payerSelect");
const errorDiv = document.createElement("div");
const errorText = document.createTextNode('*Please fill out all required fields');

// additional variables
let currencyNames = [];
let payerNames = [];
let chosenWorthOption;
let chosenPayerOption;
let flag = true;

// code to run
applyDesignChanges();
limitKeypressOnInputs();
createCurrencyNamesArray(worthWrapper, currencyNames, worthSelect);
if (payerWrapper) {
    createCurrencyNamesArray(payerWrapper, payerNames, payerSelect);
}
editSubmit.addEventListener("click", () => {
    validationBeforeSubmit();
    assignWorthForSubmit();
});
// end

function applyDesignChanges() {
    payeeInput.placeholder = 'Please type username of payee here';
    removeBrFromContentElement();
    cleanMarginOnPElement();
    if (!payerWrapper) {
        payerWrapperTwig.style.display = "none";
    }
}

function limitKeypressOnInputs() {
    payeeInput.setAttribute(
        "onkeypress",
        "return limitKeypress(event, payeeInput.value, 20)"
    );
    descriptionInput.setAttribute(
        "onkeypress",
        "return limitKeypress(event, descriptionInput.value, 32)"
    );
}

function valueRemover(value, index, subindex) {
    if (value.children[index].children[subindex]) {
        return value.children[index].children[subindex].value = undefined;
    }
}

function assignWorthForSubmit() {
    for (let value of [...worthWrapper.children]) {
        valueRemover(value, 1,0);
        valueRemover(value, 1,1);
        let option = extractString(value);
        if (option === chosenWorthOption) {
            value.children[1].children[0].value = worthInteger.value;
            value.children[1].children[1].value = worthDecimalValue(worthDecimal.value);
        }
    }
}

function assignPayerForSubmit() {
    for (let value of [...payerWrapper.children]) {
        if (value.children[1].innerHTML === chosenPayerOption) {
            value.children[0].checked = true;
        }
    }
}

function onWorthChange() {
    if (
        chosenWorthOption !== undefined &&
        chosenWorthInteger !== undefined &&
        chosenWorthDecimal !== undefined
    ) {
        assignWorthForSubmit();
    }
}

function onPayerChange() {
    if (chosenPayerOption !== undefined) {
        assignPayerForSubmit();
    }
}

function removeBrFromContentElement() {
    for (let i = br.length; i--;) {
        br[i].parentNode.removeChild(br[i]);
    }
}

function cleanMarginOnPElement() {
    for (let i = p.length; i--;) {
        p[i].style.margin = "0";
    }
}

function extractString(value) {
    let str = value.innerText.replaceAll('\n', '');
    return str.replace(' .', '');
}

function createCurrencyNamesArray(element, array, select) {
    for (let value of [...element.children]) {
        let option = extractString(value);
        array.push(option);
    }
    appendOptionsToSelect([...element.children], array, select);
}

function appendOptionsToSelect(children, array, select) {
    for (let i = 0; i < children.length; i++) {
        let option = document.createElement("option");
        option.setAttribute("value", array[i]);
        option.text = array[i];
        if (option.children !== "undefined") {
            select.appendChild(option);
        }
    }
}

function limitKeypress(event, value, maxLength) {
    if (value !== undefined && value.toString().length >= maxLength) {
        event.preventDefault();
    }
}

function validationReset() {
    flag = true;
    errorDiv?.remove();
    errorText?.remove();
    let allElements = [payeeInput, worthInteger, worthDecimal, worthSelect, descriptionInput]
    if (payerWrapper) {
        allElements.push(payerSelect);
    }
    for (let element of allElements) {
        element.style.border = 'solid #ccc 1px';
    }
}

function validationIfEmptyString() {
    let inputElements = [payeeInput, worthInteger, worthDecimal, descriptionInput];
    for (let element of inputElements) {
        if (element.value === '') {
            element.style.border = 'solid red 1px';
            flag = false;
        }
    }
}

function validationIfSelected() {
    let selectElements = [worthSelect];
    if (payerWrapper) {
        selectElements.push(payerSelect);
    }
    for (let element of selectElements) {
        if (element.children[0].selected) {
            element.style.border = 'solid red 1px';
            flag = false;
        }
    }
}

function appendError() {
    errorDiv.style.margin = "0 auto 0 200px";
    errorDiv.style.color = 'red';
    errorDiv.appendChild(errorText);
    editActions.insertBefore(errorDiv, editSubmit);
}

function validationBeforeSubmit() {
    validationReset();
    validationIfEmptyString();
    validationIfSelected();

    if (flag === false) {
        appendError();
        event.stopPropagation();
        event.preventDefault();
    }
}

function worthDecimalValue(value) {
    switch (value.length) {
        case 1: return value + '000';
        case 2: return value + '00';
        case 3: return value + '0';
        case 4: return value;
    }
}
