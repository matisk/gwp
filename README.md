# README 
## powinno być czytane w viewerze żeby kopiować prawidłowe wartości (np. na gitlab, patrz sekcja GIT)

## Kolory:
> #### biały: _#ffffff_
> #### czarny: _#000000_
> #### szary: _#f7f9fa_
> #### niebieski: _#0d77b5_
> #### niebieski-jasny: _#2494db_
> #### niebieski-ciemny: _#0f6292_
> #### czrwony: _#b51e0d_

## Block layout:
> #### Gdzie:
> _/admin/structure/block_ 
> #### Co zrobić:    
> blok **_Primary admin actions_** dodać do sekcji **Sidebar**. W części **Visibility** -> **Pages** dodać **_*/transfer/transfer_**

> #### Gdzie:
> _/admin/structure/block_
> #### Co zrobić:
> blok **_Private Message Actions_** dodać do sekcji **Sidebar**. W części **Visibility** -> **Pages** dodać **_*/private-messages_**

## Formularz "Transfer":
> #### Gdzie:
> _/admin/accounting/transactions/form-display/transfer_
> #### Co zrobić:
> w sekcji **USER EXPERIENCE** -> **Main form Twig** dodaj zawartość **_transfer.html.twig_** z katalogu **transfer** w **Button label** wklej _**Transfer**_

## JavaScript
> #### Gdzie:
> _/admin/config/development/asset-injector/js_
> #### Co zrobić:
> kliknij "**Add Js Injector**", na następnej stronie: 
> 1. w **label** wklej _custom_
> 2. w **code** wklej zawartość pliku _customXXX.js_ z folderu **transfer/js**, gdzie XXX wskazuje dla jakiego systemu jest przeznaczyny plik
> 3. w sekcji **Condition** -> **Pages** -> wklej **_*/transfer/transfer_** -> zaznacz opcję **Show for the listed pages**

## CSS:
> #### Gdzie:
> _/admin/config/development/asset-injector/css_
> #### Co zrobić:
> kliknij '**Add CSS Injector**', na następnej stronie:
> 1. w **label** wklej _cssName_
> 2. w **code** wklej zawartość pliku _cssName.css_ z katalogu **css**
> 3. w sekcji **Condition** -> **Pages** -> wklej **_whichPages_** -> zaznacz opcję **Show for the listed pages**
> > Lista plików css:
> > - _**cssName**_: custom
> > - _**cssName**_: transfer, _**pages**_: */transfer/transfer
> > - _**cssName**_: save, _**pages**_: */save
> > - _**cssName**_: ad, _**pages**_: */ad/\*
> > - _**cssName**_: smallads, _**pages**_: */smallads, */smallads/offer, */smallads/want
> > - _**cssName**_: contact, _**pages**_: */contact
> > - _**cssName**_: private-message, _**pages**_: */private-messages

## GIT:
> https://gitlab.com/matisk/gwp

## smallads:
> #### Gdzie:
> na serwerze dla dev w folderze: _/var/www/dev.wpoprzek.eu/web/modules/contrib/smallads/templates_
> #### Co zrobić:
> podmienic plik _smallad.html.twig_, poinien mieć prawa '_**chmod 444**_'

> #### Gdzie:
> na serwerze dla dev w folderze: 
> _/var/www/dev.wpoprzek.eu/web/sites/default/files/php/twig/627fb0ca6add9_smallad.html.twig_6z_gkoaaCqo-UbZ6hjzO2zK7P_
> #### Co zrobić:
> usunąć _**plikiODługichSkomplikoawnychNazwach.php**_ i zostawić tylko _**.htaccess**_ po odświerzeniu strony */ad/* 
> utworzy się nowy plik skompilowany przez Drupal'a

> #### Gdzie:
> _/admin/config/development/asset-injector/css_
> #### Co zrobić:
> jeśli "**CSS Injector**" dla pliku ad.css jeszcze nie istnieje to kliknij '**Add CSS Injector**', na następnej stronie:
> 1. w **label** wklej _ad_
> 2. w **code** wklej zawartość pliku _ad.css_ z folderu _**smallad**_
> 3. w sekcji **Condition** -> **Pages** -> wklej **_whichPages_** -> zaznacz opcję **Show for the listed pages**

## Private messages:
> #### Gdzie:
> na serwerze dla dev w folderze: _/var/www/dev.wpoprzek.eu/web/modules/contrib/private_message/images_
> #### Co zrobić:
> podmienic plik _private-message-notification-icon.png_ na dołączonego o tej samej nazwie w folderze **private-messages**

## Strony błędów: 403 i 404 :
> #### Gdzie:
> _/admin/content_
> #### Co zrobić:
> kliknąć **+ Add content**, potem wybrać **Basic Page**, jak pojawi się okno edytora to 
> dać tytuł: dla 403 - **Error 403** kliknąc **Source** w edytorze Body (żeby umożliwić dodawanie czystego HTML) i 
> przekleić zawartość content_pages/403.html a w Sidebar w zakładce **URL ALIAS** wkleić _/403_

> #### Gdzie:
> _/admin/config/system/site-information_
> #### Co zrobić:
> w sekcji **ERROR PAGES** dodać link do utworzonej strony (np. _/403_) - Drupal powinien automatycznie przerobić je na linki do node'ów 

## Strony w footerze:
> #### Gdzie:
> _/admin/content_
> #### Co zrobić:
> 1. kliknij **+ Add content**,
> 2. na następnej stronie wybrać **Basic Page**,
> 3. w edytorze z lewej strony kliknąc **Source**,
> 4. na dole edytora w select **Text format** wybrać _Full HTML_
> 5. w **Title** wklej _title_
> 6. w **Body** wklej zawartość pliku z folderu **content_pages** _fileName.html_
> 7. po prawej w dropdown'ie **URL ALIAS** wklej w inlut **URL alias** _urlAlias_
> > Lista plików html z folderu **content_pages**:
> > - _**title**_: Account set up, _**fileName**_: account_set_up, _**urlAlias**_: \/public/setup,
> > - _**title**_: Terms of Use, _**fileName**_: terms_of_use, _**urlAlias**_: \/public/tou,

## Sidebar - Wallet
> #### Gdzie:
> _/admin/accounting/misc_
> #### Co zrobić:
> Sekcja **Delimiter** powinna być pusta


I agree to Terms of Use of wpoprzek.eu
template
